let gulp = require('gulp'),
	spawn = require('child_process').spawn,
	node;
let ts = require('gulp-typescript');
const header = require('gulp-header');
/**
 * $ gulp server
 * description: launch the server. If there's a server already running, kill it.
 */
gulp.task('server', ['build'], function() {
	if (node) node.kill()
	node = spawn('node', ['dist/index.js'], {
		stdio: 'inherit'
	})
	node.on('close', function(code) {
		if (code === 8) {
			gulp.log('Error detected, waiting for changes...');
		}
	});
})

gulp.task('build', function() {
	return gulp.src('src/**/*.ts')

		.pipe(ts({
			lib: ["es6", "dom"],
			typeRoots: [
				"../node_modules/json-d-ts"
			],
			resolveJsonModule: true,
			module: "commonjs",
			noImplicitReturns: true,
			sourceMap: true,
			target: "es6",
		}))

		.pipe(gulp.dest('dist/'));
});

gulp.task('default', ['server'], function() {
	gulp.watch(['./src/**', './src/**/*'], ['server'])
	gulp.watch(["src/modules/socket/apiCalls.ts"], ['apiCalls']);

	// Need to watch for sass changes too? Just add another watch call!
})

// clean up if an error goes unhandled.
process.on('exit', function() {
	if (node) node.kill()
})


gulp.task('apiCalls', function() {
	return gulp.src(['src/modules/socket/apiCalls.ts'])
	.pipe(header('// DONT CHANGES THIS FILE\n// DONT CHANGES THIS FILE\n// DONT CHANGES THIS FILE\n// DONT CHANGES THIS FILE\n'))
	.pipe(gulp.dest('../src/app/core/models/'));
});
