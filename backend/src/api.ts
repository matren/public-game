import * as express from 'express';
import * as bodyParser from 'body-parser';

class Api {
	public express;

	constructor() {
		this.express = express();
		this.addMiddlewares();
		this.mountSubRoutes();
		this.mountApiRoutes();
		this.errorRoutes();
	}

	private addMiddlewares(): void {
		this.express.use(bodyParser.urlencoded({ extended: true }));
		this.express.use(bodyParser.json());

		// Prevent CORS error and sanitize
		this.express.use((req, res, next) => {
			res.header('Access-Control-Allow-Origin', '*');
			res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Email, User');
			if (req.method === 'OPTIONS') {
				res.header('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');
				return res.status(200).json({});
			}
			next();
		});
	}

	private mountSubRoutes(): void {
		// this.express.use("/pfad", var);
	}

	private mountApiRoutes(): void {
		const apiRoutes = express.Router();
		apiRoutes.get('/', (req, res) => {
			res.json({
				message: 'Wellcome to the API' + req.body
			});
		});
		this.express.use('/', apiRoutes);
	}

	private errorRoutes(): void {
		this.express.use((req, res, next) => {
			const error: { status?: number, message: string } = new Error('Route not available:');
			error.status = 404;
			next(error);
		});

		this.express.use((error, req, res) => {
			res.status(error.status || 500);
			res.json({
				error: {
					message: error.message,
					data: req.body
				}
			});
		});
	}
}

export default new Api().express;
