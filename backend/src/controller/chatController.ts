import * as moment from 'moment';
import { ChatMessage } from '../modules/models/chatmessage';
import { CircularBuffer } from '../modules/models/circularBuffer';
import { io } from '..';
import api from '../modules/socket/apiCalls';

class ChatController {

	
	public lastGlobalMessages: CircularBuffer<ChatMessage> = new CircularBuffer<ChatMessage>(50);

	constructor() {

	}
	
	public addGlobalMessage(chatMessage: ChatMessage): void {
		chatMessage.date = new Date().getTime();
		this.lastGlobalMessages.push(chatMessage);
		console.log('test', this.lastGlobalMessages.get());

			
		io.chatSocket.emit.broadCast(api.chat.response.sendGlobalMessage, chatMessage);

	}

	public getAllGlobalMessages(): ChatMessage[] {
		return this.lastGlobalMessages.get();
	}

}

export default new ChatController();
