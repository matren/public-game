import * as moment from "moment";

class HelpController {
	constructor() {

	}

	public getDayDiff(a, b): number {
		let ret = b - a;
		if (ret < 0)
			ret += 7;

		return ret;
	}

	public getTimeDiff(a, daysDiff: number): number {
		let overhang = 0;
		const hour = moment(a).hour();
		const min = moment(a).minute();
		const nowHour = moment().hour();
		const nowMin = moment().minute();
		let diffHour = nowHour - hour;
		let diffMin = nowMin - min;
		if (diffHour < 0 && daysDiff === 0) {
			diffHour += 24;
			overhang += 6*24*60;
		}
		if (diffMin < 0) {
			diffMin += 60;
		}
		return diffHour*60 + diffMin + overhang;
	}
}

export default new HelpController();
