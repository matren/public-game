// import * as admin from 'firebase-admin';
import ChatSocket from './chatsocket/chatSocket';
import { Server } from 'http';

// const serviceAccount = require('../../firebasekey.json');

// admin.initializeApp({
// 	credential: admin.credential.cert(serviceAccount)
// });


class Socket {
	public socket: any;
	public chatSocket: ChatSocket;

	constructor(server: Server) {
		this.socket = require('socket.io')(server);
		this.addMiddleware();
		this.mountSubRoutes();
	}

	private addMiddleware(): void {
		// this.socket.use((socket, next) => {
		// 	const tokenId = socket.handshake.query.token;
		// 	const type = socket.handshake.query.type;
		// 	admin.auth().verifyIdToken(tokenId).then(user => {
		// 		socket.uid = user.uid;
		// 		return next();
		// 	}).catch(err => {
		// 		return next(new Error('Authentication error'));
		// 	});
		// });
	}

	private mountSubRoutes(): void {
		this.chatSocket = new ChatSocket(this.socket.of('/chat'));
	}
}

export default Socket;
