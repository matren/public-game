
import config from '../../../config';
import api from '../apiCalls';
const request = api.chat.request;
const response = api.chat.response;
import chatController from '../../../controller/chatController';
import { ChatMessage } from '../../models/chatmessage';

class Handler {
	private io;
	constructor(socket) {
		this.io = socket;
	}

	public handle(call, socket, data): void {
		if (request.hasOwnProperty(call)) {
			try {
				console.log('frontend call:', call);
				this[call](socket, data);
			} catch (e) {
				this.handleError(socket, e);
			}
			if (config.debug)
				socket.emit('methodCalled', call);
		} else {
			this.handleError(socket, null, 'invalid api call ' + call);
		}
	}

	public broadcast(socket, data: ChatMessage): void {
		chatController.addGlobalMessage(data);
	}


	private handleError(socket, err, msg = null): void {
			msg = err.stack;
			console.log('Fehler', err);
			socket.emit('exception', {error: msg});
	}

}





export default Handler;
