import api from '../apiCalls';
import Handler from './handler';
import Emitter from './emitter';
import helper from '../../db/helper';
// import * as admin from 'firebase-admin';


// admin.initializeApp({
// 	credential: admin.credential.cert(serviceAccount)
// });


class ChatSocket {
	public socket: any;
	public emit: Emitter;

	constructor(socket) {
		this.socket = socket;
		this.configureHandler();
		this.configureEmitter();
	}

	private configureHandler(): void {
		const handler = new Handler(this.socket);
		this.socket.on('connection', (socket) => {
			console.log('ChatSocket user connected');

			for (const call in api.chat.request) {
				if (api.chat.request.hasOwnProperty(call)) {
					socket.on(call, (data) => handler.handle(call, socket, data));
				}
			}

			socket.on('disconnect', () => {
				console.log('ChatSocket user disconnect');
			});
		});
	}

	private configureEmitter(): void {
		this.emit = new Emitter(this.socket);
	}
}

export default ChatSocket;
