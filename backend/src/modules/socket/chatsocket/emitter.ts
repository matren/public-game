
import helper from "../../db/helper";
import api from "../apiCalls";
const request = api.chat.request;
const response = api.chat.response;

class Emitter {
	private io;
	constructor(socket) {
		this.io = socket;
	}

	// public async fansChanged(fans: Fan[]): Promise<void> {
	// 	this.io.emit(response.fansChanged, fans);
	// }

	public broadCast(event: string, obj: any): Promise<void> {
		console.log('broadCast', event, obj);
		return this.io.emit(event, obj);
	}

}


export default Emitter;
