export class CircularBuffer<T> {

	private array: T[] = [];
	private pointer: number = -1;

	constructor(size: number) {
		this.array = new Array(size);
		
	}

	public get(): T[] {
		return this.array;
	}

	public getElement(i: number): T {
		return this.array[i];
	}

	public push(elem: T): void {
		if (this.pointer + 1 < this.array.length)
			this.pointer++;
		else
			this.pointer = 0;
		this.array[this.pointer] = elem;
	}
}