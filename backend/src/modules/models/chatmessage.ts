export class ChatMessage {
	public firebaseid: number;
	public name: string;
	public text: string;
	public channel: number;
	public date: number;
	
	constructor(
		firebaseid: number,
		name: string,
		text: string,
		channel: number,
		date: number = 0,
	) {
		this.firebaseid = firebaseid;
		this.name = name;
		this.text = text;
		this.channel = channel;
		this.date = date;
	}
}
