
export class Item {
	public id: number;
	public name: string;
	public tooltip: string;
	public maxStacks: number;
	constructor(
		id,
		name,
		tooltip,
		maxStacks,
	) {
		this.id = id;
		this.name = name;
		this.tooltip = tooltip;
		this.maxStacks = maxStacks;
	}
}

export class GatherItem extends Item {
	
}

export class UserableItem extends Item {
	public type: string;

	constructor(
		id: number,
		name: string,
		tooltip: string,
		maxStacks: number,
		type: string) {
		super(id, name, tooltip, maxStacks);
		this.type = type;
	}
}

export class EquipItem extends Item {
	public bonus: string;
	public slot: number;
	public usedByClass: number[];
	constructor(
		id: number,
		name: string,
		tooltip: string,
		maxStacks: number,
		bonus: string,
		slot: number,
		usedByClass: number[]
		) {
		super(id, name, tooltip, maxStacks);
		this.bonus = bonus;
		this.slot = slot;
		this.usedByClass = usedByClass;
	}
}