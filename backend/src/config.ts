const config = {
	portBackend: process.env.ENV_PORT || 4321,
	portFrontend: process.env.ENV_PORTFRONTEND || 44321,
	mode: process.env.NODE_ENV || 'dev',
	debug: true,

	host: 'localhost',
	username: 'root',
	password: '',
	databasename: 'database',
};

process.argv.forEach((item, i) => {
	if (i < 2) return;
	for (const key in config) {
		if (config.hasOwnProperty(key)) {
			if (item.indexOf(key+'=') === 0) {
				config[key] = item.substr(key.length+1);
			}
		}
	}
});

export default config;
