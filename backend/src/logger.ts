import * as winston from 'winston';
import * as DailyRotateFile from 'winston-daily-rotate-file';
import * as moment from 'moment';


class Logger {
	private logger;
	constructor() {
		this.logger = winston.createLogger({
			level: 'info',
			format: winston.format.json(),
			transports: [new winston.transports.Console(),
			new DailyRotateFile({
				// frequency: "24h",
				dirname: './logs',
				filename: 'log-%DATE%.log',
				datePattern: 'YYYY-MM-DD'
			})]
		});
	}

	private getDate(): string {
		return moment().format('DD.MM.YYYY - HH:mm:ss') + ' ';
	}

	public debug(text: string, obj?: any): void {
		if (obj !== undefined)
			this.logger.log('debug', text+': '+JSON.stringify(obj));
		else
			this.logger.log('debug', text);
	}

	public info(text: string, obj?: any): void {
		if (obj !== undefined)
		this.logger.info(this.getDate()+text+': '+JSON.stringify(obj));
	else
		this.logger.info(this.getDate()+text);
	}

	public warning(text: string, obj?: any): void {
		if (obj !== undefined)
		this.logger.warn(this.getDate()+text+': '+JSON.stringify(obj));
	else
		this.logger.warn(this.getDate()+text);
	}

	public error(text: string, obj?: any): void {
		if (obj !== undefined)
		this.logger.error(this.getDate()+text+': '+JSON.stringify(obj));
	else
		this.logger.error(this.getDate()+text);
	}
}


export default new Logger();
