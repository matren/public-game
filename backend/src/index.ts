
import Socket from './modules/socket/socket';
import api from './api';
import view from './view';
import config from './config';
import http = require('http');
import * as moment from 'moment';
import 'moment/locale/de';

const httpServer = http.createServer(api);
const frontendView = http.createServer(view);

moment.locale('de');

httpServer.listen(config.portBackend, () => {


	api._router.stack.forEach(function(r) {
		if (r.route && r.route.path) {
			console.log(r.route.path);
		}
	});

	return console.info(`HTTP server is listening on ${config.portBackend} in \'${config.mode}\' mode`);
});

frontendView.listen(config.portFrontend, () => {


	view._router.stack.forEach(function(r) {
		if (r.route && r.route.path) {
			console.log(r.route.path);
		}
	});

	return console.info(`HTTPS Frontendserver is listening on ${config.portFrontend} in \'${config.mode}\' mode`);
});


export const io = new Socket(httpServer);
