

import * as express from 'express';
import * as bodyParser from 'body-parser';
import serveStatic = require('serve-static');

class View {
	public express;

	constructor() {
		this.express = express();
		this.addMiddlewares();
		this.mountApiRoutes();
		// this.errorRoutes();
	}

	private addMiddlewares(): void {
		this.express.use(bodyParser.urlencoded({ extended: true }));
		this.express.use(bodyParser.json());

		// Prevent CORS error and sanitize
		this.express.use((req, res, next) => {
			res.header('Access-Control-Allow-Origin', '*');
			res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, Email, User');
			if (req.method === 'OPTIONS') {
				res.header('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE');
				return res.status(200).json({});
			}
			next();
		});

	}


	private mountApiRoutes(): void {
		console.log(__dirname + '/view');
		this.express.use(serveStatic(__dirname + '/view/', { dotfiles: 'allow' }));
		// this.express.use("node_modules/", serveStatic(__dirname + "node_modules/", { dotfiles: "allow" }));
		// this.express.use('/.well-known/acme-challenge/cToQqFKSNJOjUGP6xP3aS7BudVzRFsMIe-0sDYhUxpg', serveStatic(__dirname + "/view/.well-known/acme-challenge/cToQqFKSNJOjUGP6xP3aS7BudVzRFsMIe-0sDYhUxpg", { dotfiles: "allow" }));

	}

	private errorRoutes(): void {
		this.express.use((req, res, next) => {
			const error: { status?: number, message: string } = new Error('Route nicht verfügbar');
			error.status = 404;
			next(error);
		});

		this.express.use((error, req, res) => {
			res.status(error.status || 500);
			res.json({
				error: {
					message: error.message,
					data: req.body
				}
			});
		});
	}
}

export default new View().express;
