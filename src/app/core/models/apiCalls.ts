// DONT CHANGES THIS FILE
// DONT CHANGES THIS FILE
// DONT CHANGES THIS FILE
// DONT CHANGES THIS FILE
const api = {
	chat: {
		request: { // eintreffende calls zum backend
			initMe: 'initMe',
			broadcast: 'broadcast'
		},

		response: { // ausgehende calls richtung frontend
			init: 'init',
			sendGlobalMessage: 'sendGlobalMessage',
		}
	}

};


export default Object.freeze(api);



