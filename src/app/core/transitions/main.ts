import {
	trigger,
	state,
	style,
	animate,
	transition,
} from '@angular/animations';

export const opacityLoading = trigger('opacityLoading', [
	state('true', style({
		opacity: 1,
	})),
	state('void', style({
		opacity: 0,
	})),
	transition(':enter', [
		animate('1s')
	]),
	transition(':leave', [
		animate('.5s 1s')
	]),
]);


export const toggleHeightBlob = trigger('toggleHeightBlob', [
	transition(':enter', [
		style({ transform: 'scale(0.5) translateY(80px)', opacity: 0 }),  // initial
		animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)',
			style({ transform: 'scale(1)', opacity: 1 }))  // final
	]),
	transition(':leave', [
	style({ transform: 'scale(1)', opacity: 1, height: '*' }),
	animate('1s cubic-bezier(.8, -0.6, 0.2, 1.5)',
		style({
			transform: 'scale(0.5)', opacity: 0,
			height: '0px', margin: '0px'
		}))
	])
]);
