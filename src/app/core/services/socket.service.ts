import { Injectable, NgZone } from '@angular/core';
import io from 'socket.io-client';
import { environment } from 'src/environments/environment';
import ChatSocket from './socket/chatsocket';
import MainSocket from './socket/mainSocket';
import api from '../models/apiCalls';
import { Subject } from 'rxjs';
import { ChatMessage } from 'backend/src/modules/models/chatmessage';


@Injectable({
	providedIn: 'root'
})
export class SocketService {

	public chatSocket: ChatSocket = new ChatSocket();

	constructor() {

	}

	public async connect(token: string): Promise<void> {
		console.log('connect all sockets', );
		await this.chatSocket.connect(token);
	}

	public async send(socketName: string, path: string, object: any): Promise<void> {
		let socket: MainSocket;
		switch (socketName) {
			case 'chat': socket = this.chatSocket; break;
		}
		if (!socket)
			throw new Error('not connected to socketio');
		if (!this.validPath(path))
		throw new Error('path: "' + path + '" is not a valid path');

		return socket.send(path, object);
	}

	
	public isConnected(): boolean {
		return !!this.chatSocket;
	}

	public validPath(path: string): boolean {
		for (const r of Object.keys(api.chat.request)) {
			if (r === path)
				return true;
		}
		return false;
	}
	
}
