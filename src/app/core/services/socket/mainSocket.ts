import { environment } from 'src/environments/environment';
import io from 'socket.io-client';
import api from '../../models/apiCalls';

abstract class MainSocket {

	protected socket: any;
	
	constructor() {
		// this.connect(token);
	}

	public connect(token: string): void {
		this.socket = io.connect(environment.backendUrl + '/chat', {
			secure: true,
			reconnect: true,
			rejectUnauthorized: false,
			query: {
				token
			}
		});
		Object.keys(api.chat.response).forEach(key => {
			this.socket.on(key, (data) => {
				this.handle(key, data);
			});
		});
		this.socket.on('exception', (data) => this.handleError(data));
		console.log(this.constructor.name, 'is connected');
	}
	
	protected handle(key, data): void {
		console.log('from backend', key, data);
		try {
			this[key](data);
		} catch (e) {
			console.log('FEHLER (existiert event?)', e);
		}
	}
	
	// public abstract send(path: string, object: any): void;
	public send(path: string, object: any): void {
		console.log(this.constructor.name, 'send:', path, ' + ', object);
		this.socket.emit(path, object);
	}

	public handleError(err): void {
		console.log('FEHLER VOM BACKEND', err);
	}

}
export default MainSocket;
