import { environment } from 'src/environments/environment';
import io from 'socket.io-client';
import MainSocket from './mainSocket';
import { ChatMessage } from 'backend/src/modules/models/chatmessage';
import { Observable, Subject, Subscribable, Subscriber, ReplaySubject, BehaviorSubject } from 'rxjs';

class ChatSocket extends MainSocket {

	public globalMessageEvent: Subject<ChatMessage> = new Subject();
	
	constructor() {
		super();
	}

	public sendGlobalMessage(chatMessage: ChatMessage): void {
		console.log('push', );
		this.globalMessageEvent.next(chatMessage);
	}

}
export default ChatSocket;
