import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// modules
import { AppRoutingModule } from './app-routing.module';
import { LoginModule } from './modules/login/login.module';
import { NotificationModule } from './modules/notification/notification.module';

// components
import { AppComponent } from './app.component';
import { GameModule } from './modules/game/game.module';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		LoginModule,
		NotificationModule,
		BrowserAnimationsModule,
		GameModule,
		
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
