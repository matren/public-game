import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { NotificationService } from 'src/app/modules/notification/notification.service';
import { LoadingService } from 'src/app/modules/loading/loading.service';
import { SocketService } from 'src/app/core/services/socket.service';
import { Router } from '@angular/router';

@Component({
selector: 'login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	public email: string;
	public password: string;

	constructor(
		private authService: AuthService,
		private notificationService: NotificationService,
		@Inject(LoadingService) protected loadingService: LoadingService,
		private socketService: SocketService,
		private router: Router,
	) { }

	ngOnInit() {
	}

	public async login(type: string): Promise<void> {
		// let userCredential: firebase.auth.UserCredential;
		// this.loadingService.start();
		// try {
		// switch (type) {
		// 	case 'anonymous':
		// 		userCredential = await this.authService.signInAnonymous();
		// 		break;
		// 	case 'email':
		// 		userCredential = await this.authService.signInWithEmail(this.email, this.password);
		// 		break;
		// 	case 'registeremail':
		// 		userCredential = await this.authService.registerWithEmail(this.email, this.password);
		// 		break;
		// 	case 'google':
		// 		userCredential = await this.authService.signInWithGoogle();
		// 		break;
		// 	case 'facebook':
		// 		userCredential = await this.authService.signInWithFacebook();
		// 		break;
		// 	case 'twitter':
		// 		userCredential = await this.authService.signInWithTwitter();
		// 		break;
		// 	}
		// } catch (e) {
		// 	this.loadingService.stop();
		// 	console.log('FEHLER', e);
		// 	this.notificationService.showNotification(e);
		// }

		// // connect to socket
		// const token: string = await userCredential.user.getIdToken(/* forceRefresh */ true);
		const token: string = '123';
		await this.socketService.connect(token);
	
		// this.loadingService.stop();
		// if (userCredential && userCredential.user && userCredential.user.uid) {
			// geh weiter zu root
		this.router.navigate(['/']);
		// }
	}

}
