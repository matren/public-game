import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';

import { environment } from '../../../environments/environment';
// import { AngularFireModule } from 'angularfire2';
// import { AngularFireDatabaseModule } from 'angularfire2/database';
// import { AngularFireAuthModule } from 'angularfire2/auth';
import { FormsModule } from '@angular/forms';
import { LoadingModule } from '../loading/loading.module';

// serivces
import { AuthService } from './services/auth.service';

@NgModule({
	declarations: [
		LoginComponent
	],
	imports: [
		CommonModule,
		// AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'),
		// AngularFireDatabaseModule,
		// AngularFireAuthModule,
		FormsModule,
		LoadingModule,
	],
	providers: [
		AuthService,
	]
})
export class LoginModule { }
