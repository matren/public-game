import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

// import { AngularFireAuth } from 'angularfire2/auth';
// import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { SocketService } from 'src/app/core/services/socket.service';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	// private user: Observable<firebase.User>;

	private loggedIn: boolean = false;
	private connected: boolean = false;

	constructor(
		// private firebaseAuth: AngularFireAuth,
		private socketService: SocketService,
		private router: Router,
	) {
		// this.user = firebaseAuth.authState;


		// this.firebaseAuth.auth.onAuthStateChanged((user: firebase.User) => {
		// 	if (user) {
				console.log('user is logged in');
				this.loggedIn = true;
				// user.getIdToken().then(token => {
					this.socketService.connect('123').then(data => {
						this.connected = true;
					});
				// });
		// 	} else {
		// 		console.log('user is not logged in');
		// 		this.router.navigate(['/login']);
		// 	}
		// });
	}

	// public signInWithEmail(email: string, password: string): Promise<firebase.auth.UserCredential> {
	// 	// return this.firebaseAuth.auth.signInWithEmailAndPassword(email, password);
	// }
	// public registerWithEmail(email: string, password: string): Promise<firebase.auth.UserCredential> {
	// 	// return this.firebaseAuth.auth.createUserWithEmailAndPassword(email, password);
	// }

	// public signInWithGoogle(): Promise<firebase.auth.UserCredential> {
	// 	return this.firebaseAuth.auth.signInWithPopup(
	// 		new firebase.auth.GoogleAuthProvider()
	// 	);
	// }

	// public signInWithFacebook(): Promise<firebase.auth.UserCredential> {
	// 	return this.firebaseAuth.auth.signInWithPopup(
	// 		new firebase.auth.FacebookAuthProvider()
	// 	);
	// }

	// public signInWithTwitter(): Promise<firebase.auth.UserCredential> {
	// 	return this.firebaseAuth.auth.signInWithPopup(
	// 		new firebase.auth.TwitterAuthProvider()
	// 	);
	// }


	// public signInAnonymous(): Promise<firebase.auth.UserCredential> {
	// 	return this.firebaseAuth.auth.signInAnonymously();
	// }

}
