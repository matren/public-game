import { Component, OnInit, Inject } from '@angular/core';
import { LoadingService } from '../loading.service';
import { opacityLoading } from 'src/app/core/transitions/main';

@Component({
	selector: 'loading',
	templateUrl: './loading.component.html',
	styleUrls: ['./loading.component.scss'],
	animations: [
		opacityLoading
	]
})
export class LoadingComponent implements OnInit {

	public cubeClass: string[] = ['stretch-right', 'shrink-left', 'shrink-top'];
	public cubeClasses: string[] = [
		'stretch-right', 'shrink-right', 'stretch-top', 'shrink-top', 'strech-left', 'shrink-left', 'stretch-bottom', ''
	];
	// public cubeClasses: string[] = ['shrink-left', 'stretch-bottom'];
	private counter: number = 0;

	constructor(
		@Inject(LoadingService) protected loadingService: LoadingService,
		) { }

	ngOnInit() {

		setInterval(() => {
			if (!this.loadingService.isLoading) { return; }
			this.counter++;
			if (this.counter > 5) {
				this.counter = 0;
			}
			const i = Math.floor((this.counter) / 2);
			const cube = this.cubeClass[i];

			let index = this.cubeClasses.indexOf(cube) + 1;
			if (index >= this.cubeClasses.length) {
				index = 0;
			}
			this.cubeClass[i] = this.cubeClasses[index];

		}, 1000);
	}

}
