import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notification/notification.component';
import { NotificationFactoryComponent } from './notification-factory/notification-factory.component';


@NgModule({
	declarations: [
		NotificationComponent,
		NotificationFactoryComponent
	],
	imports: [
		CommonModule,
	],
	exports: [
		NotificationFactoryComponent,
	]
})
export class NotificationModule { }
