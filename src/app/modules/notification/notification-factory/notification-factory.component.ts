import { Component, OnInit, Inject } from '@angular/core';
import { INotificationService, NotificationService, INotificationDeclaration } from '../notification.service';
import { toggleHeightBlob } from 'src/app/core/transitions/main';

@Component({
	selector: 'notification-factory',
	templateUrl: './notification-factory.component.html',
	styleUrls: ['./notification-factory.component.scss'],
	animations: [
		toggleHeightBlob,
	]
})
export class NotificationFactoryComponent implements OnInit {

	public messages: INotificationDeclaration[] = [];

	constructor(
		@Inject(NotificationService) protected notificationService: INotificationService
		) { }

	ngOnInit(): void {


		this.notificationService.notifications.subscribe((message: INotificationDeclaration) => {
			this.messages.splice(0, 0, message);

			if (message.duration > 0) {
				setTimeout(() => this.removeNotification(message), message.duration);
		}
		});
	}


	public removeNotification(message: INotificationDeclaration): void {
		const index: number = this.messages.indexOf(message);
		if (index !== -1) {
				this.messages.splice(index, 1);
		}
	}

	public closeMessage(message: INotificationDeclaration): void {
		this.removeNotification(message);
	}

}
