import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

export interface INotificationDeclaration {
  id: number;
  message: string;
  type: string;
  duration: number;
}
export interface INotificationService {
  notifications: Observable<INotificationDeclaration>;
  showNotification(message: string, type?: string, duration?: number): void;
}

@Injectable({
  providedIn: 'root'
})
export class NotificationService implements INotificationService {

  private recentNotificationId: number = 0;
  public notifications: Subject<INotificationDeclaration> = new Subject<INotificationDeclaration>();

  constructor() { }

  public showNotification(message: string, type: string = 'info', duration?: number): void {
    if (type !== 'error' && duration === undefined) {
      duration = 4000;
    } else if (duration === undefined) {
      duration = 0;
    }
    this.notifications.next({
      id: this.recentNotificationId++,
      message,
      type,
      duration
    });
  }
}
