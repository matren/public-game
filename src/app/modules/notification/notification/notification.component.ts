import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { INotificationDeclaration } from '../notification.service';

@Component({
  selector: 'notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  @Input()
  public message: INotificationDeclaration;

  @Output()
  public closeMessage: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit() {
  }

  public onCloseMessage() {
    this.closeMessage.emit();
  }

}
