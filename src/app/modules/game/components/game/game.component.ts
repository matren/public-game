import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/modules/login/services/auth.service';
import { Router } from '@angular/router';

@Component({
	selector: 'game',
	templateUrl: './game.component.html',
	styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

	constructor(
		private authService: AuthService,
		private router: Router,
		) {}

	public async ngOnInit(): Promise<void> {
		
		
	}

}
