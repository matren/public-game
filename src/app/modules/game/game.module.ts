import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './components/game/game.component';
import { InventoryModule } from '../inventory/inventory.module';
import { ChatModule } from '../chat/chat.module';

@NgModule({
	declarations: [GameComponent],
	imports: [
		CommonModule,
		InventoryModule,
		ChatModule,
	]
})
export class GameModule { }
