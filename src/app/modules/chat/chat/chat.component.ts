import { Component, OnInit, ElementRef, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { SocketService } from 'src/app/core/services/socket.service';
import { ChatMessage } from 'backend/src/modules/models/chatmessage';
import api from '../../../core/models/apiCalls';
import { Observable, BehaviorSubject } from 'rxjs';

@Component({
	selector: 'chat',
	templateUrl: './chat.component.html',
	styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

	public globalChatMessages: ChatMessage[] = [];
	public globsub: BehaviorSubject<ChatMessage[]> = new BehaviorSubject<ChatMessage[]>([]);

	public chatText: string = '';
	public blabla: string = '';


	constructor(
		private socketService: SocketService,
		private cdr: ChangeDetectorRef
	) { }

	ngOnInit() {
		// this.globl = this.socketService.chatSocket.globalMessageEvent;
		this.socketService.chatSocket.globalMessageEvent.subscribe((chatMessage: ChatMessage) => {
			// this.blabla += 'blabla';
			this.globalChatMessages.push(chatMessage);
			console.log('message kommt sofort:', this.globalChatMessages);
			this.globsub.next(this.globalChatMessages);
		});

	}

	public enterText(evt: KeyboardEvent): void {
		if (evt.code === 'Enter') {
			const message = this.chatText;
			this.socketService.send('chat', api.chat.request.broadcast, 
				new ChatMessage(123, '', message, 0)
			);
			this.chatText = '';
		}
	}
}
