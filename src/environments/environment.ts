// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
	production: false,
	firebase: {
		apiKey: 'AIzaSyCCep5UoUfy6krr_cpd5HCHkEbYoicrZxo',
		authDomain: 'clickandloot-cd04a.firebaseapp.com',
		databaseURL: 'https://clickandloot-cd04a.firebaseio.com',
		projectId: 'clickandloot-cd04a',
		storageBucket: '',
		messagingSenderId: '809927898605',
		appId: '1:809927898605:web:865932f27b94641c',
	},
	backendUrl: 'http://localhost:4321',

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
